# YouTube Vanced+ [placeholder]
**Modification of YouTube/YouTube Music app for Android with ad-free, background playback and many other tweaks made by Cuynu!**

</p>
 <a href="https://telegram.me/vxupdate" ><img src="https://img.shields.io/badge/YouTube Vanced+ Channel-2CA5E0?style=for-the-badge&logo=telegram&logoColor=dark"></a>
<p align="left">
  <a href="https://discord.gg/U7z2hsxbyM">
    <img alt="Discord" src="https://img.shields.io/discord/1077997663628296333?color=%2300C853&label=YouTube%20VancedX%20Server&logo=discord&logoColor=%2300C853&style=for-the-badge">
  </a>

## Table of Contents

* [When Vanced+ will be available?](#well)
* [Why this project still exists even ReVanced was a replacement of YouTube Vanced ?](#why-this-project-still-exists-even-revanced-was-a-replacement-of-youtube-vanced-)
* [Credits](#credits)
* [Features](#features)
* [Building YouTube Vanced+ from source](https://gitlab.com/cuynu/vancedx/-/wikis/Building)
* [Download YouTube Vanced+ APKs](#download)
* [Troubleshoot](https://gitlab.com/cuynu/vancedx/-/wikis/Troubleshoot)
* [Source code](#source-code)

## Introduction 
This project was created after discontinuation of Vanced official aswell wars between Unofficial Vanced and ReVanced Extended. Its not `YouTube Vanced`. The project are in development and will going release soon as possible!

## Features 
- **YouTube Vanced+ blocks ads from YouTube and uses SponsorBlock to skip in-video sponsor segments**
- **The picture-in-picture mode allows watching videos in a floating window**
- **Background play allows playing video sound in background**
- **Override max resolution**
- **Swipe control for brightness and volume**
- **Google login like the original YouTube app using Vanced+ MicroG**
- **Dislike counter re-added using the Return YouTube Dislike database**
- **Disable YouTube Shorts function everywhere**
- **Enable old layout of YouTube**
- **Download videos from YouTube using external downloader app**
- **Custom video speed**
- **Enable YouTube Premium header (not actually enable Premium features!)**
- *Many more...*

## Wait...

**Why i couldn't find any Vanced+ APKs or its source code, does this project abandoned ?**

We are tired of "ReVanced Extended/ReVanced" community cuz of their measures to force we to don't do anything related to Vanced+ development and only want "ReVanced Extended/ReVanced/ReX" to be exists as their only YouTube for Android mods, that why this project literally abandoned months ago. The Vanced+ source code (patches/integrations/cli) are private until new announcement. (ofc if Vanced+ ready to release, its will come with prebulit + source code). 
Thanks for visting this project and we hope one day we can back and continue to implement Vanced+ !


## Download  

### You won't be able to download **YouTube Vanced+** as this project are not publicly available yet, the repository and the download section are just "placeholder". 
### Instead, you can try old Unofficial Vanced by clicking "Older version" on download section below, sorry for this inconvenient.

Notice : It is recommended to build Vanced+ yourself instead of using pre-bulit apk or module, follow **[this instruction](https://gitlab.com/cuynu/vancedx/-/wikis/Building)** to build for yourself. if you can't build or lazy, use pre-bulit apk below, its have all patches included :)

-------------------------

### YouTube Vanced+ non-root variant 

**[Download latest version of Vanced+ MicroG](https://gitlab.com/cuynu/VancedxMicroG/-/releases)**

Current Version : **19.12.36** | **[Older version](https://gitlab.com/cuynu/vancedx/-/releases)**

Minimum Android version : **9+ (Pie)**


### Black Theme 

**[Download Black theme variant for arm64-v8a](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Black%20Theme/)**

**[Download Black theme variant for armeabi-v7a](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Black%20Theme/)**

**[Download Black theme variant for x86](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Black%20Theme/)**

**[Download Black theme variant for x86_64](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Black%20Theme/)**

**[Download Black theme variant for Universal](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Black%20Theme/)**

-------------------------

### Dark Theme

**[Download Dark theme variant for arm64-v8a](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Dark%20Theme/)**

**[Download Dark theme variant for armeabi-v7a](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Dark%20Theme/)**

**[Download Dark theme variant for x86](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Dark%20Theme/)**

**[Download Dark theme variant for x86_64](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Dark%20Theme/)**

**[Download Dark theme variant for Universal](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Dark%20Theme/)**

-------------------------

### Material You Theme (Android 12+)
 
**[Download Material You theme variant for arm64-v8a](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Material%20You%20Theme/)**

**[Download Material You theme variant for armeabi-v7a](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Material%20You%20Theme/)**

**[Download Material You theme variant for x86](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Material%20You%20Theme/)**

**[Download Material You theme variant for x86_64](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Material%20You%20Theme/)**

**[Download Material You theme variant for Universal](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Non-root%20Packages/Vanced%2B%2019.01.32/Material%20You%20Theme/)**


-------------------------

### YouTube Vanced+ root variant (Magisk/KernelSU) 

Notice : Install **[detach module](https://github.com/j-hc/zygisk-detach/releases)** to prevent Play Store from update and replace installed Vanced+. If you are using microG services core as replacement for GMS, enable `Fix video playback issue` on Vanced+ settings -> Video to fix buffering issue !

Current Version : **19.01.32** | **[Older version](https://gitlab.com/cuynu/vancedx/-/releases)**

Minimum Android version : **9+ (Pie)**

### Black Theme

**[Download Black theme variant for Universal](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Root%20Packages%20%28Magisk%20or%20KSU%20Module%29/Vanced%2B%2019.01.32/Black%20Theme/)**

-------------------------

### Dark Theme 

**[Download Dark theme variant for Universal](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Root%20Packages%20%28Magisk%20or%20KSU%20Module%29/Vanced%2B%2019.01.32/Dark%20Theme/)**

-------------------------

### Material You Theme (Android 12+)
 
**[Download Material You theme variant for Universal](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced%2B/Vanced%2B%20Root%20Packages%20%28Magisk%20or%20KSU%20Module%29/Vanced%2B%2019.01.32/Material%20You%20Theme/)**


-------------------------

### YouTube Music Vanced+ non-root variant

**[Download latest version of Vanced+ MicroG](https://gitlab.com/cuynu/VancedxMicroG/-/releases)**

Current Version : **6.28.52** | **[Older version](https://gitlab.com/cuynu/vancedx/-/releases)**

Minimum Android version : **9+ (Pie)**


[![Download YouTube Vanced+ Prebulit ](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced+/Music%20Vanced%2B%20Non-root%20Packages)

-------------------------

### YouTube Music Vanced+ root variant

Notice : Install **[detach module](https://github.com/j-hc/zygisk-detach/releases)** to prevent Play Store from update and replace installed Vanced+.

[![Download YouTube Vanced+ Prebulit ](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/vancedx/files/Prebulit%20Vanced+/Music%20Vanced%2B%20Root%20Packages%20(Magisk%20or%20KSU%20Module))


-------------------------

## Source code

**Official YouTube app itself are proprietary and closed source, we can't access YouTube source code because its are private which only Google/YouTube developer can see its original code in kotlin and java which is not obfuscated and modify it. So we can only patch and modify YouTube from published compiled binary apk which is extremely obfuscated by Google/YouTube developer when they compiling YouTube app. Here is source code for what was modified and all of Vanced+/Vanced features, again, DONT ask for YouTube app source code! :**

#### [View source code of YouTube Vanced+ (patches)](https://gitlab.com/cuynu/vancedx-patches)

#### [View source code of YouTube Vanced+ (integrations)](https://gitlab.com/cuynu/vancedx-integrations)

#### [View source code of YouTube Vanced+ (cli)](https://gitlab.com/cuynu/vancedx-cli)

-------------------------

## Contribute  

Users can contribute translation or new features to this project, but remember put your contributed features to "Community features" section on `Vanced+ settngs -> Extra` instead of other section and make sure these feature must be **DISABLED** by default ! 

Contributors :

- Cuynu : maintainer
- [Syuugo](https://github.com/s1204IT) : help with japanese translation for Vanced+ microG

## Why this project still exists even ReVanced was a replacement of YouTube Vanced ?
- This project was renamed to "YouTube Vanced+", basically i just add + after Vanced, but its still is a different project than original Vanced or ReVanced. ReVanced are too different from original Vanced and has too many useless feature that enabled by default, specially ReVanced Extended (eg : Hide suggested actions, Hide Subscription tab, Hide every YouTube components that not ADS make original YouTube experience impacted), our goal is continuing Vanced as Vanced+ branding without breaking original YouTube experience, all of features that NOT related to ADS, Downloader or Playback/PIP will be disabled by default
- That means when you install and open YouTube Vanced+ first time, all you see is a YouTube app with no ADS but without any layout modifications, unlike how ReVanced does. you still can modify layout on Vanced+ settings -> Layout but we will never set these tweaks as ON by default.
- Also we still provide pre-bulit YouTube Vanced+ app for who can't patch for yourself just or lazy. Unlike ReVanced (official channel), they never provide pre-bulit app and requires you to bulit from source
- ReVanced Extended are discontinued again recently, so its time for us!
## Well...
- I don't know when Vanced+ will available to anyone, but for now this project still being developed on PRIVATE REPOSITORY. Vanced+ always been my dream, but most ppls include u knows, i have bad memories about Unofficial Vanced and bad programming skill, that why its still not available yet

## Credits

**[Team Vanced](https://github.com/TeamVanced)** : Old YouTube Vanced official which is closed source

**[inotia00](https://github.com/inotia00)** : Old YouTube Vanced (RVX) based patches (17.34.36-18.21.34)

**[ReVanced Team](https://github.com/revanced)** : ReVanced Team

-------------------------

## [Go back to top of this page](#youtube-vanced-placeholder)

